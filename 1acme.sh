#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1acme.sh' encrypted at Wed Jan 31 02:36:05 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SYZ��: ��������߯����������������� _K`����t       �"� ��Z��	DA24	='�6F�L�O��O$��M����#OI��#A�z�M�  �&�MS��O525����4OI�4<� 4����   @  UL�4�M4�4���	�4m aF&C@0F�i��@ѣ@4Ѡ@�Ѧ�24d44�M �2h�ɓ#L�L�  a0�&##L�0�!������      �         � H�� �h44�	�����������i�OD�23SF��  �� �4q0$ �&���Vk�|=��\9,v�gȮ6<Dg��,ct���y��VL�(d�Ĺ� T_#4�6��LӬܔ�;/�?��eϓ�7K�Zy���عX��ěyԂD���Sܺ	,�t�v�oV᧞r��Z���$ 2(k��L��.�ۅp_��:%U�=��%�H�Sl�b����ٯ7>)(���Z�a�S%.��Zg7.*�SU���:E��h�<�﫵4���=+�р��������d-:2X�C6|%�Ƭh����wD�W��+04ݗ���� +�<�L����%Z���i��cJ��6�� ���D*�k3\Qh������sq���P�=��%�J��趑be_~Z6J���åh��?������Pe�Z�n"�Ȥ�0�r�$5,Q%�b���
�RS�HD̊�tʊ{�u�p�'��Ōk�c<{�y��^- ��b�p��F�jln�ЩW�>̉RՊ����'+]�iC��]�Ax[���R@8�BS��
����Z�(t���n&�0(g���GG���������1hG�_Hă��w��
�ٮ���d��PƔ�M��[Ol)ج&��Y��*aeL��p}��fS�f�Z!'�2qeT�����^P,�����:�Z�3�ow�� d�ķm�Dz� ���I������\��n�m�0��h�}��������m�(�}�\�-���|Q3���t���������fr�F���J� ������Њ�ڳͰ�!6�&Z���4y�0�i���DT���!yflx��+JIf%�!+g��~,<�0�  ����������;���ny�m�2b��Հ��U��#��5	��Z�Y3(����8A��G�������ơ��y5�e������y�2�;b�m_�~u�5�p�n���gG�mZ��Z��;y,�3������РK�\�E	��$�~����/�X��G�-Y��/Q=O��Gt�ج:$i�K�I6!��f鿛���qj-i=���B�*@�M�F	e�p�i�N�@{�',�P�	7&���Y ��l�-���4�����IFܿ�t(��Sb	����f�E�[�A����RT����3���j8�7������=�*tʲ�GL�pu����PJUv�* : X�}c�qko�配����k� pmW��m9zjW�
)$L�Gő���A)I��~$��kJ�#��([Y�Q$^�!1�e�zܟ?��N8�
�*z�ց���'9�ռ�����u��n�@���.�1,+����մdlӫo� =P1�Qr˘<-!k{~~��� �^1%�YZl0�V%�8��R���;=�DDKl��\�.�}���N�iŐ��9f��P=Uq�M��d�˞�<~+O�����v�<���;D���	7�Sh�)�����P� �$ ���SAK������@�(�Ŭ����'W�$��8զʒ��@k��p��%�	���uK�U�)=,���S�79٤�Ay��a�)I-�B�Y�����N�Z�����!+Z<� $&"��|:�����Ӓ�`�^X�h%��w�F�H�qº��Wa]D��ͣ0�3�\~���/�@٨�CJ���������	'T���ҕX�C��6.��E�UH�Kċ��Z,���)+���}�
�ጒ�900���%��J��(��t6�h@���$�T�7F^��d������J��鵫z=ʣ�3I�L�u@�fr��"�k��U~@�&���5��1"��|���6(�m���U�J��GT(��N���]��i���)�i�ts��-@0M�9��A�h_z�	]6�e"�p�q���b�X��BqU
��a�1 ����{|���KH�I���^�|_��恴��r1��{��c=�,&�3���:G�1}��א �D4��rTA1�_%&35�$t,�͢r3Ğo�Y��9��-A���*�םhW�����B���0��MHe~0G���:`Sy7���2ʬ�R�)(�@��0Q�m	pz�ii�=>;R�KXz��zI�� 6�7�0��N�Rgf�J[C R�)n3�ڂ���Aj�7�!�i}^��[�A�ә��Zŕ!6��If�|9�@�@����
{f��;	�Ti{#�G �@��ah
�/�ċD�j��R��:X���A���-�m�t��^��X��&@Ό��/���H
,(��s�g4h죵Ur��)�I(��QC�g�GL���g!I��ei��ʪ⨮GgL	�@�,D�&��aj+�bC@{&1���2cB����
Ho@9�Ěe�I"�̼SI\%�	.�^�\(�"�i	p$���Q�K٨/<@p�� (!�ch�4�* $ ���,�ʳX���>�A��8�0�dCK���a�*�~+9#A�EI���D@��EI{����H�qUG`).�|^����#
�s�-�����}� �z-�c8*��GKAA���F�B1�I5��T��	g�3�T�4����r�+g��P6be
Մ%޸Ƶ%h6��ZȄ613!�-�Bk�u���A�b7z��3�'�%�RkB.��7�ɬ�aZ�Б�R]A�{��	^�Åp# �2(P�0F#����#A�Z�pdI.��i�`4��j�Е!12F�d�7l�j���6`+IZ#hS	�
	bH�&�Z�Z�&6i����ӄ�XQ�l�$��R7jK�V�w	h��z�ְ�m���`O�2e@���}9x<����A �r$�#���Gw{h ZF���&Q2������� '+��(��hm�"E��
H��A��d"��&�X�:��D�RLiV��"����9����E�A�'�(Z�h�CB!���	��d ����S\LHŨR��ƻ���Kԍ���?5�����a�2��U�ToM%����j�;gg�V�	Z��JA�&�~�� �c�hI�'g
j���6n-�I�`�)42�u�����R���[�3��.E��v��X.p�<)0�r��K��D�ź����Z4�� 	��˂�U h����HcY&�1g̦qaI.0ʵ��� �T����	���ʘ��B�8,ٱ�EjB� #��U൬*�e��Jb�  d&��3|/f"�a9�)MS��be��
�ƯK$&ؒ�#�dW��Z��1
��Ih�*!�(c�j0�0�߮�{4��u"�����:��V��#�]�FJ�0*�B!���1<�ERicH�M�I�d�&AEP�Ԇ�Ä�H
�p�&�I�!)+L+I�StEG��6j�dA�`	��eA��+*$W�͡���aN�����j��g��P>�4��t:�n��L�x��F�F0��	��/ԥ$u1��`�02��y
�d���p�k�cYc�ܭ�D��w�����ec���3#�����������;��L�zMa�c.Ln�L��@F��#JJtԃh����Akm���-ݚŉt���dB>J�� ��F.NS����Go� �5�?�X��/}�����r!��
Rֹ��fg� �(�F�-�w�[��~�"I��	�|�X�������!���
<��6��1�d�1����&��'�hG� �4�����c%b�)�m��i"?V�0�3\䄙��^%x�$��$ p��ffNL�1@��EF�[�@��s����:G��֮R��G���t�p�t�a��l�AM
��Kf5CH�v��"cD�U ��ԡ�)JhƝ��i`jJc���劉�II���ι�%H@�M�sVZ$�I^�8��T%(�1���f�,���]�L��\?�s/�9�f��?ZH8P��Hj����T����S��ἔG}!|���gf�����(7l9gVt.6.xZ6��ET5"�la����BT� �B�
�$��n��1���1�p��r��T$��&6:P`�G�6���x6	j[������R�[&4*	B��!B̉Ǭ(�hgx����{�=�
���iNUx�K/H�$�.��55�P�K�Z�0Iv�q��nkXf�Bh�l4��J��G��0�C�ܑ��$�$�X��X���UϿ�����`�$X�!@[UO��	��ޱ$�	�����i�Y�'����E�R��}� ���QUYQ�6x�h3�P�ٱ1����.Ȏ ��Q�2�ֽ�\�<2�����8�E�Wh���zy�`���<4.lR�D�v*��i1V컼v�tG� ��1 ���Z�$ԃ��̻��XV�}�}�r.E�ab,C��(v�>Q��m��B�?b	46(cM�cc4|��>ә�V�h��c���q������_j�ץ���d-�|�QW3$��(��MB01^Dw�S��H �AՈ���P���ğ>@Kk DB�p辘
�]�u��դuښ�]�� ���,�9���+	~I� �K豔�����kO��x�S�3c�8Ց9��NZ��\%���'ߖ!�{�_�#/��{������@�P*���qHR+�K�1���L�b�W��+�B�l
��	V�Y�扊A��Nl���E��`�n��	/�U�]�' �1	)Cg���Aq��wr$�P�Z�q�n��gx�6�dq���l��l�Ҋ$u��W��A�Rʪ˘���ٺ�a	%�5YLk\��%ڔ�o/�&���6)Ffk�Q7hz����*�*�7 ׉�P��t���PeČO
Z�MI�����^u�68����?e�,X�o�F6Mwvx�%}�,�;{�������L/U��v��4��?6U0�i��RG�:��lFL�h+Gi�ti�����+�H��N����}��4%�7J�f��o��+��<�0�@6(�;���dx���˙"fޞ~����Ӯ��}�C���I�iU�
qT�^j!��@I������)c$1bw�r,8�_I_�*I��/[�=��]vDd��㜽pf�ޭ5��l0�@�S�$�`Lu9 �X��ǀ�w�2�>gO�3�]4%��Z2.�$����Y9��o��jN�.�g�N���X��F�0�v3�u-�w�4t�hC����P��)[����o>�ݛ8�c��!�6Ƈ1G���VZ�����a)�y�:lN�@���$�=:{TnZN\�>!������/���)�ԗ��